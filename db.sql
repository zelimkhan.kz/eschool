PGDMP     $    ,                v            school    10.5    10.5     �
           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �
           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �
           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �
           1262    16744    school    DATABASE     �   CREATE DATABASE school WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Kazakh_Kazakhstan.1251' LC_CTYPE = 'Kazakh_Kazakhstan.1251';
    DROP DATABASE school;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �
           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �
           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    24946    students    TABLE     �   CREATE TABLE public.students (
    stud_id integer NOT NULL,
    name character varying(50) NOT NULL,
    age integer NOT NULL,
    email character varying(50) NOT NULL
);
    DROP TABLE public.students;
       public         postgres    false    3            �            1259    24944    students_stud_id_seq    SEQUENCE     �   CREATE SEQUENCE public.students_stud_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.students_stud_id_seq;
       public       postgres    false    3    197            �
           0    0    students_stud_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.students_stud_id_seq OWNED BY public.students.stud_id;
            public       postgres    false    196            n
           2604    24949    students stud_id    DEFAULT     t   ALTER TABLE ONLY public.students ALTER COLUMN stud_id SET DEFAULT nextval('public.students_stud_id_seq'::regclass);
 ?   ALTER TABLE public.students ALTER COLUMN stud_id DROP DEFAULT;
       public       postgres    false    196    197    197            �
          0    24946    students 
   TABLE DATA               =   COPY public.students (stud_id, name, age, email) FROM stdin;
    public       postgres    false    197   �       �
           0    0    students_stud_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.students_stud_id_seq', 4, true);
            public       postgres    false    196            p
           2606    24951    students students_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.students
    ADD CONSTRAINT students_pkey PRIMARY KEY (stud_id);
 @   ALTER TABLE ONLY public.students DROP CONSTRAINT students_pkey;
       public         postgres    false    197            �
   )   x�3�J����42�1�3��s3s���s�b���� �
�     