1) Чтобы скачать с гитхаба проект запускаем команду:
    (in this project I used Intellij adea)
    git clone https://gitlab.com/zelimkhan.kz/eschool.git
    
2) Заходим в папку:
    (using postgresql)
    eschool/ и импортируем схему базы данных db.sql
    либо создаём такую же схему где:
        database: school
        schema: public
        username: postgres
        login: tiger
    и используем запросы для создания и инсерта в таблицу данных
    (create_insert_select.sql)
    a путь обращения в базу должен выглядеть вот так:
        url=jdbc:postgresql://localhost:5432/school
        
3) Данные о базе уже прописаны и хранятся в:
    eschool/src/main/resources/application.properties
    
4) Необходимо собрать проект используя gradle:
    запускаем таск build, после чего gradle сам соберёт
    наш проект.
    
5) После чего поместим файл eschool-1.0-SNAPSHOT.war
    в tomcat/apache-tomcat-9.0.10/webapps
    и переименуюм файл в eschool.war
    
6) Через командную строку пройдём в директорию
    tomcat/apache-tomcat-9.0.10/bin
    и выщываем команду startup.bat(если вы исользуете Windows)
    а для пользователей Linux напишите команду ./startup.sh
 
7) После чего поднимется сервер и в базе данных
будут доступны данные url:
    http://localhost:8080/eschool/login.jsp
    для входа на страницу логин
    
    http://localhost:8080/eschool/search.html
    для взода на страницу просмотра всех студентов
    и для перехода на страницу добавления студента
    
    http://localhost:8080/eschool/add.html
    форма заполнения данных студента
     
 