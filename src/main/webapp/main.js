var container = $('div.container');

$('input#get').click(function() {

    $(container).empty();

    $.ajax({
        type: 'GET',
        url: '/eschool/getAll',
        dataType: 'json',
        success: function(data) {

            $.each(data, function(index, item) {
                var id = 0;
                $.each(item, function(key, value) {
                    if(key == 'studId'){
                        id = value;
                    } else {
                        container.append(key + ': ' + value + ' ');
                    }
                });
                container.append('<a href="http://localhost:8080/eschool/delete?studid='+id+'">Delete</a></br>');
            });
        }
    });
});

$('input#add').click(function() {
    window.location.href = "add.html"
});