package services;

import models.Student;
import models.StudentMapper;
import org.apache.ibatis.session.SqlSession;
import util.MyBatisSqlSessionFactory;

import java.util.List;

public class StudentService {

    public List<Student> findAllStudents(){
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession();
        try{
            StudentMapper studentMapper = sqlSession.getMapper(StudentMapper.class);
            return studentMapper.findAllStudents();
        }finally {
            sqlSession.close();
        }
    }

    public Student findStudentsById(Integer studId){
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession();
        try{
            StudentMapper studentMapper = sqlSession.getMapper(StudentMapper.class);
            return studentMapper.findStudentById(studId);
        }finally{
            sqlSession.close();
        }
    }

    public void createStudent(Student student){
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession();
        try{
            StudentMapper studentMapper = sqlSession.getMapper(StudentMapper.class);
            studentMapper.insertStudent(student);
            sqlSession.commit();
        }finally{
            sqlSession.close();
        }
    }

    public void deleteStudent(Integer studId){
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession();
        try{
            StudentMapper studentMapper = sqlSession.getMapper(StudentMapper.class);
            studentMapper.deleteStudent(studId);
            sqlSession.commit();
        }finally {
            sqlSession.close();
        }
    }

    public void updateStudent(Student student){
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession();
        try{
            StudentMapper studentMapper = sqlSession.getMapper(StudentMapper.class);
            studentMapper.updateStudent(student);
            sqlSession.commit();
        }finally {
            sqlSession.close();
        }
    }

}
