package servlets;

import com.google.gson.Gson;
import models.Student;
import services.StudentService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/delete")
public class DeleteStudentServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public DeleteStudentServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        response.setContentType("application/json");

        StudentService studentService = new StudentService();

        int id = Integer.parseInt((String)request.getParameter("studid"));

        studentService.deleteStudent(id);

        try {
            response.sendRedirect("search.html");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
