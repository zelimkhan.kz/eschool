package servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public LoginServlet() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        try {
            System.out.println(username+" and "+password);
            if(username.equals("tiger") && password.equals("123")){
                response.sendRedirect("search.html");
            } else {
                response.sendRedirect("login.jsp");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
