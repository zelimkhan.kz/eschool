package servlets;

import com.google.gson.Gson;
import models.Student;
import services.StudentService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/getAll")
public class GetStudentsServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public GetStudentsServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        response.setContentType("application/json");

        StudentService studentService = new StudentService();
        List<Student> students = studentService.findAllStudents();

        String json = new Gson().toJson(students);

        try {
            response.getWriter().write(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
