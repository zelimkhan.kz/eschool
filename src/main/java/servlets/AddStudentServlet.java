package servlets;

import models.Student;
import services.StudentService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/add")
public class AddStudentServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public AddStudentServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        StudentService studentService = new StudentService();

        String name = (String)request.getParameter("name");
        int age = Integer.parseInt((String)request.getParameter("age"));
        String email = (String)request.getParameter("email");

        Student student = new Student(name, age, email);
        studentService.createStudent(student);

        try {
            response.sendRedirect("search.html");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
