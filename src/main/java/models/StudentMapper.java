package models;

import java.util.List;

public interface StudentMapper {

    List<Student> findAllStudents();
    Student findStudentById(Integer id);
    void insertStudent(Student student);
    void deleteStudent(Integer studentId);
    void updateStudent(Student student);

}
