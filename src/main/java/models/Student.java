package models;

import java.util.Date;

public class Student {

    private int studId;
    private String name;
    private int age;
    private String email;

    public Student(){}

    public Student(Integer studId){
        this.studId = studId;
    }

    public Student(String name, int age, String email){
        this.name = name;
        this.age = age;
        this.email = email;
    }

    public Student(int studId, String name, int age, String email) {
        this.studId = studId;
        this.name = name;
        this.age = age;
        this.email = email;
    }

    public int getStudId() {
        return studId;
    }

    public void setStudId(int studId) {
        this.studId = studId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

}